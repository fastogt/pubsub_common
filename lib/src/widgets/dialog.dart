import 'package:flutter/material.dart';

class DialogEx extends StatelessWidget {
  final IconButton? leading;
  final String? title;
  final List<IconButton>? actions;
  final EdgeInsets contentPadding;
  final Widget content;

  const DialogEx(
      {this.leading,
      this.title,
      this.actions,
      this.contentPadding = const EdgeInsets.fromLTRB(24, 4, 24, 24),
      required this.content});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        titlePadding: _padding,
        title: Row(children: [
          if (leading != null) leading!,
          if (title != null) Text(title!),
          const Spacer(),
          if (actions != null) ...actions!
        ]),
        contentPadding: contentPadding,
        content: content);
  }

  EdgeInsets? get _padding {
    if (leading == null && actions == null) {
      return null; // forces dialog to use AlertDialog default padding
    }
    return EdgeInsets.fromLTRB(leading == null ? 24 : 12, 12, 12, 0);
  }
}
