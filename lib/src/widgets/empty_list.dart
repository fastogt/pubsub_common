import 'package:flutter/material.dart';

class EmptyListBuffer extends StatelessWidget {
  final String message;
  final IconData icon;

  const EmptyListBuffer(this.message, [this.icon = Icons.find_in_page]);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Opacity(
            opacity: 0.5,
            child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 32.0),
                child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                  Icon(icon, size: 96),
                  const SizedBox(height: 24),
                  Text(message, textAlign: TextAlign.center)
                ]))));
  }
}
