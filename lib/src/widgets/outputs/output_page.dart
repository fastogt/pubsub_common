import 'package:fastocloud_dart_media_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:pubsub_common/models.dart';
import 'package:pubsub_common/src/widgets/outputs/utils.dart';
import 'package:pubsub_common/widgets.dart';

class OutputDialogTranslation {
  final String addHeader;
  final String editHeader;

  final String name;
  final String webUrl;
  final String rtmpUrl;
  final String key;
  final String emptyField;

  const OutputDialogTranslation(
      {this.addHeader = 'Add output',
      this.editHeader = 'Edit output',
      this.name = 'Name',
      this.webUrl = 'Web url',
      this.rtmpUrl = 'RTMP url',
      this.key = 'Key',
      this.emptyField = "This field can't be empty"});
}

class RtmpOutputDialog extends _RtmpOutputBase {
  const RtmpOutputDialog.add(
      {required IRtmpOutputUrl output,
      required OnPressedLinkCallback onPressedLink,
      OutputDialogTranslation translation = const OutputDialogTranslation()})
      : super.add(output: output, translation: translation, onPressedLink: onPressedLink);

  const RtmpOutputDialog.edit(
      {required IRtmpOutputUrl output,
      required OnPressedLinkCallback onPressedLink,
      OutputDialogTranslation translation = const OutputDialogTranslation()})
      : super.edit(output: output, translation: translation, onPressedLink: onPressedLink);

  @override
  _RtmpOutputDialogState createState() {
    return _RtmpOutputDialogState();
  }
}

class _RtmpOutputDialogState extends _RtmpOutputBaseState<RtmpOutputDialog> {
  @override
  Widget layout() {
    return DialogEx(
        leading: IconButton(icon: const Icon(Icons.arrow_back), onPressed: exit),
        title: header,
        actions: actions(),
        contentPadding: const EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 16.0),
        content: ScrollableEx(builder: (controller) {
          return SingleChildScrollView(controller: controller, child: fields());
        }));
  }
}

class RtmpOutputPage extends _RtmpOutputBase {
  const RtmpOutputPage.add(
      {required IRtmpOutputUrl output,
      required OnPressedLinkCallback onPressedLink,
      OutputDialogTranslation translation = const OutputDialogTranslation()})
      : super.add(output: output, translation: translation, onPressedLink: onPressedLink);

  const RtmpOutputPage.edit(
      {required IRtmpOutputUrl output,
      required OnPressedLinkCallback onPressedLink,
      OutputDialogTranslation translation = const OutputDialogTranslation()})
      : super.edit(output: output, translation: translation, onPressedLink: onPressedLink);

  @override
  _RtmpOutputPageState createState() {
    return _RtmpOutputPageState();
  }
}

class _RtmpOutputPageState extends _RtmpOutputBaseState<RtmpOutputPage> {
  @override
  Widget layout() {
    return Scaffold(
        appBar: AppBar(
            leading: IconButton(icon: const Icon(Icons.arrow_back), onPressed: exit),
            title: Text(header),
            actions: actions()),
        body: SingleChildScrollView(
            child: Padding(padding: const EdgeInsets.symmetric(horizontal: 8.0), child: fields())));
  }
}

abstract class _RtmpOutputBase extends StatefulWidget {
  final bool isAdd;
  final IRtmpOutputUrl output;
  final OutputDialogTranslation translation;
  final OnPressedLinkCallback onPressedLink;

  const _RtmpOutputBase.add(
      {required this.output, required this.translation, required this.onPressedLink})
      : isAdd = true;

  const _RtmpOutputBase.edit(
      {required this.output, required this.translation, required this.onPressedLink})
      : isAdd = false;
}

abstract class _RtmpOutputBaseState<T extends _RtmpOutputBase> extends State<T> {
  late IRtmpOutputUrl outputUrl;
  late String? root;
  late String? key;

  String get header => widget.isAdd ? widget.translation.addHeader : widget.translation.editHeader;

  @override
  void initState() {
    super.initState();
    outputUrl = widget.output.copy();
    root = outputUrl.root();
    key = outputUrl.key();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          exit();
          return Future.value(true);
        },
        child: layout());
  }

  Widget layout();

  List<IconButton> actions() {
    return [
      if (!widget.isAdd) IconButton(icon: const Icon(Icons.delete), onPressed: delete),
      IconButton(icon: const Icon(Icons.save), onPressed: save)
    ];
  }

  Widget fields() {
    return Column(children: [
      TextFieldEx(
          init: outputUrl.name,
          hintText: widget.translation.name,
          onFieldChanged: (String value) {
            outputUrl.name = value;
          }),
      TextFieldEx(
          init: outputUrl.webUrl,
          hintText: widget.translation.webUrl,
          decoration: const InputDecoration().copyWith(suffixIcon: webUrlButton()),
          onFieldChanged: (String value) {
            outputUrl.webUrl = value;
          }),
      TextFieldEx(
          init: root,
          hintText: widget.translation.rtmpUrl,
          errorText: widget.translation.key,
          onFieldChanged: (String value) {
            root = value;
          }),
      TextFieldEx(
          init: key,
          hintText: widget.translation.key,
          errorText: widget.translation.key,
          onFieldChanged: (String value) {
            key = value;
          })
    ]);
  }

  Widget webUrlButton() {
    return IconButton(
        tooltip: 'Launch URL',
        icon: const Icon(Icons.launch),
        onPressed: () {
          widget.onPressedLink.call(outputUrl.webUrl);
        });
  }

  void closeKeyboard() {
    FocusScope.of(context).unfocus();
  }

  void exit() {
    closeKeyboard();
    Navigator.of(context).pop(EditResult<IRtmpOutputUrl>(widget.output, EditType.UNCHAGED));
  }

  void delete() {
    closeKeyboard();
    Navigator.of(context).pop(EditResult<IRtmpOutputUrl>(widget.output, EditType.DELETE));
  }

  void save() {
    final stabled = IRtmpOutputUrl.generateUrl(root, key);
    if (stabled == null) {
      return;
    }

    closeKeyboard();
    outputUrl.uri = stabled;
    Navigator.of(context).pop(EditResult<IRtmpOutputUrl>(outputUrl, EditType.SAVE));
  }
}
