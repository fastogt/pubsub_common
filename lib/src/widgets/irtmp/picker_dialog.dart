import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:pubsub_common/widgets.dart';

class IRtmpPickerDialog extends StatelessWidget {
  final int id;
  final String title;

  const IRtmpPickerDialog(this.id, {this.title = 'RTMP template'});

  @override
  Widget build(BuildContext context) {
    return DialogEx(
        title: !kIsWeb ? translate(context, title) : title,
        actions: [IconButton(icon: const Icon(Icons.close), onPressed: Navigator.of(context).pop)],
        content:
            SizedBox(width: double.maxFinite, child: IRtmpPicker(id, Navigator.of(context).pop)));
  }
}
