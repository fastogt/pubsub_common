import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class RtmpIcon extends StatelessWidget {
  final String link;
  final double height;
  final double width;
  static const DEFAULT_SIZE = 56.0;

  const RtmpIcon(this.link, {this.height = DEFAULT_SIZE, this.width = DEFAULT_SIZE});

  const RtmpIcon.square(this.link, {double a = DEFAULT_SIZE})
      : height = a,
        width = a;

  @override
  Widget build(BuildContext context) {
    final image = CachedNetworkImage(
        imageUrl: link,
        placeholder: (context, url) {
          return const Center(child: CircularProgressIndicator());
        },
        errorWidget: (context, url, error) {
          return SizedBox.fromSize(size: Size.square(height), child: const Icon(Icons.warning));
        },
        height: height,
        width: width);
    return ClipRect(child: image);
  }
}
