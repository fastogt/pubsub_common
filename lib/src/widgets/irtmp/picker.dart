// ignore_for_file: avoid_positional_boolean_parameters

import 'package:fastocloud_dart_media_models/models.dart' hide Size;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:pubsub_common/models.dart';
import 'package:pubsub_common/widgets.dart';

typedef IRtmpPickerCallback = void Function(IRtmpOutputUrl);

class IRtmpPicker extends StatefulWidget {
  final IRtmpPickerCallback onTap;
  final bool scrollableTags;
  final int id;

  const IRtmpPicker(this.id, this.onTap, [this.scrollableTags = true]);

  @override
  _IRtmpPickerState createState() {
    return _IRtmpPickerState();
  }
}

class _IRtmpPickerState extends State<IRtmpPicker> {
  List<Tag> _tags = [];

  List<IRtmpOutputUrl> get filteredOutputs {
    final List<IRtmpOutputUrl> _filtered = [];
    for (final item in getRtmpOutList(widget.id)) {
      if (item.containsTags(_tags)) {
        _filtered.add(item);
      }
    }

    if (_filtered.isEmpty) {
      return [CustomRtmpOut(id: widget.id, uri: '')];
    }

    return _filtered;
  }

  @override
  void initState() {
    super.initState();
    _tags = Tag.values;
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      _buildTags(),
      const SizedBox(height: 4),
      Expanded(child: IRtmpUrlPickerGrid(filteredOutputs, widget.onTap))
    ]);
  }

  Widget _buildTags() {
    final chips = List<IRtmpTagChip>.generate(Tag.values.length, (index) {
      return _buildTag(Tag.values[index]);
    });
    if (widget.scrollableTags) {
      return SingleChildScrollView(scrollDirection: Axis.horizontal, child: Row(children: chips));
    }
    return Wrap(children: chips);
  }

  IRtmpTagChip _buildTag(Tag tag) {
    final selected = _tags.contains(tag);
    return IRtmpTagChip(tag, selected, (tag) {
      if (selected) {
        _tags.remove(tag);
      } else {
        _tags.add(tag);
      }
      setState(() {});
    });
  }
}

class IRtmpTagChip extends StatelessWidget {
  final Tag tag;
  final Function(Tag tag) onTap;
  final bool selected;

  const IRtmpTagChip(this.tag, this.selected, this.onTap);

  @override
  Widget build(BuildContext context) {
    final color =
        selected ? Theme.of(context).primaryColor : Theme.of(context).unselectedWidgetColor;
    final name = !kIsWeb ? translate(context, tag.toHumanReadable()) : tag.toHumanReadable();
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 4.0),
        child: ActionChip(
            label: Text(name, style: TextStyle(color: backgroundColorBrightness(color))),
            backgroundColor: color,
            onPressed: () {
              onTap(tag);
            }));
  }
}

class IRtmpUrlPickerGrid extends StatelessWidget {
  static const TILES_PADDING = 8.0;
  final List<IRtmpOutputUrl> outputs;
  final IRtmpPickerCallback onTap;

  const IRtmpUrlPickerGrid(this.outputs, this.onTap);

  @override
  Widget build(BuildContext context) {
    if (kIsWeb) {
      return ScrollableEx.withBar(builder: content);
    }
    return content();
  }

  Widget content([ScrollController? controller]) {
    return SingleChildScrollView(
        controller: controller,
        child: SizedBox(
            width: double.maxFinite,
            child: LayoutBuilder(builder: (context, constraints) {
              final int tilesCount =
                  (constraints.maxWidth / (IRtmpPickerTile.CARD_WIDTH + TILES_PADDING)).floor();
              final double tileWidth =
                  (constraints.maxWidth - (tilesCount - 1) * TILES_PADDING) / tilesCount;
              return Wrap(
                  spacing: TILES_PADDING,
                  runSpacing: TILES_PADDING,
                  children: List<IRtmpPickerTile>.generate(outputs.length, (index) {
                    return IRtmpPickerTile(outputs[index], onTap, width: tileWidth);
                  }));
            })));
  }
}

class IRtmpPickerTile extends StatelessWidget {
  static const double CARD_WIDTH = 96.0;
  final IRtmpOutputUrl output;
  final IRtmpPickerCallback onTap;
  final double width;

  const IRtmpPickerTile(this.output, this.onTap, {this.width = CARD_WIDTH, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final name = !kIsWeb && output.type == PubSubStreamType.CUSTOM
        ? translate(context, output.name)
        : Localization.toUtf8(output.name);
    return Card(
        margin: const EdgeInsets.all(0),
        elevation: 2,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
        clipBehavior: Clip.antiAlias,
        child: InkWell(
            onTap: () {
              onTap(output);
            },
            child: SizedBox(
                width: width,
                child: Column(mainAxisSize: MainAxisSize.min, children: [
                  Container(color: Colors.black54, child: icon),
                  Padding(padding: const EdgeInsets.all(8.0), child: Text(name, softWrap: true))
                ]))));
  }

  Widget get icon {
    if (output.type == PubSubStreamType.CUSTOM) {
      return SizedBox.fromSize(size: Size.square(width), child: const Icon(Icons.settings));
    }
    return RtmpIcon.square(output.icon, a: width);
  }
}
