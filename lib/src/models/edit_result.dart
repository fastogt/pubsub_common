enum EditType { SAVE, DELETE, UNCHAGED }

class EditResult<T> {
  final EditType action;
  final T value;

  EditResult(this.value, this.action);
}
