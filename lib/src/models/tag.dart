import 'package:fastocloud_dart_media_models/models.dart';
import 'package:pubsub_common/src/models/rtmp_urls_factory.dart';

class Tag {
  final int _value;

  const Tag._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == 0) {
      return 'Gaming';
    } else if (_value == 1) {
      return 'Adult';
    } else if (_value == 2) {
      return 'Education';
    } else if (_value == 3) {
      return 'Music';
    } else if (_value == 4) {
      return 'Religion';
    } else if (_value == 5) {
      return 'Vlog';
    }
    return 'Vlog';
  }

  factory Tag.fromInt(int value) {
    if (value == 0) {
      return GAMING;
    } else if (value == 1) {
      return ADULT;
    } else if (value == 2) {
      return EDUCATION;
    } else if (value == 3) {
      return MUSIC;
    } else if (value == 4) {
      return RELIGION;
    } else if (value == 5) {
      return VLOG;
    }
    return VLOG;
  }

  static List<Tag> get values => [GAMING, ADULT, EDUCATION, MUSIC, RELIGION, VLOG];

  static const Tag GAMING = Tag._(0);
  static const Tag ADULT = Tag._(1);
  static const Tag EDUCATION = Tag._(2);
  static const Tag MUSIC = Tag._(3);
  static const Tag RELIGION = Tag._(4);
  static const Tag VLOG = Tag._(5);
}

extension IRtmpTags on IRtmpOutputUrl {
  bool containsTags(List<Tag> tags) {
    for (final Tag tag in tags) {
      for (final Tag t in this.tags!) {
        if (t._value == tag._value) {
          return true;
        }
      }
    }
    return false;
  }
}
