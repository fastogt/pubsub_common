import 'package:fastocloud_dart_media_models/models.dart';
import 'package:pubsub_common/src/models/tag.dart';

List<IRtmpOutputUrl> getRtmpOutList(int id) {
  return [
    CustomRtmpOut(id: id, uri: ''),
    AfreecaTVRtmpOut(id: id, uri: ''),
    BiliBiliRtmpOut(id: id, uri: ''),
    BongaCamsRtmpOut(id: id),
    BreakersTVRtmpOut(id: id, uri: ''),
    Cam4RtmpOut(id: id),
    CamPlaceRtmpOut(id: id),
    CamSodaRtmpOut(id: id, uri: ''),
    ChaturbateRtmpOut(id: id),
    DLiveRtmpOut(id: id),
    DouyuRtmpOut(id: id, uri: ''),
    FacebookRtmpOut(id: id),
    FC2RtmpOut(id: id, uri: ''),
    Flirt4FreeRtmpOut(id: id, uri: ''),
    GoodGameRtmpOut(id: id),
    HuyaRtmpOut(id: id),
    KakaoTVRtmpOut(id: id, uri: ''),
    MixerRtmpOut(id: id, uri: ''),
    MyFreeCamsRtmpOut(id: id),
    NaverTVRtmpOut(id: id, uri: ''),
    NimoTVRtmpOut(id: id),
    OdnoklassnikiRtmpOut(id: id),
    PeriscopeRtmpOut(id: id, uri: ''),
    PicartoRtmpOut(id: id, uri: ''),
    SmashCastRtmpOut(id: id),
    SteamRtmpOut(id: id, uri: ''),
    SteamMateRtmpOut(id: id),
    StreamRayRtmpOut(id: id, uri: ''),
    StripchatRtmpOut(id: id),
    TwitchRtmpOut(id: id, uri: ''),
    VaughnLiveRtmpOut(id: id, uri: ''),
    VimeoRtmpOut(id: id),
    VirtWishRtmpOut(id: id),
    VkontakteRtmpOut(id: id, uri: ''),
    VLiveRtmpOut(id: id, uri: ''),
    XLoveCamRtmpOut(id: id, uri: ''),
    YouTubeRtmpOut(id: id),
    ZhanqiTVRtmpOut(id: id)
  ];
}

extension RtmpIconExt on IRtmpOutputUrl {
  String get icon {
    if (this is AfreecaTVRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/afreecatv.png';
    } else if (this is BiliBiliRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/bilibili.png';
    } else if (this is BongaCamsRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/bongacams.png';
    } else if (this is BreakersTVRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/breakerstv.png';
    } else if (this is Cam4RtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/cam4.png';
    } else if (this is CamPlaceRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/camplace.png';
    } else if (this is CamSodaRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/camsoda.png';
    } else if (this is ChaturbateRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/chaturbate.png';
    } else if (this is CustomRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/unknown_channel.png';
    } else if (this is DLiveRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/dlive.png';
    } else if (this is DouyuRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/douyu.png';
    } else if (this is FacebookRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/facebooklive.png';
    } else if (this is FC2RtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/fc2live.png';
    } else if (this is Flirt4FreeRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/flirt4free.png';
    } else if (this is GoodGameRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/goodgame.png';
    } else if (this is HuyaRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/huyacom.png';
    } else if (this is KakaoTVRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/kakaotv.png';
    } else if (this is MixerRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/mixer.png';
    } else if (this is MyFreeCamsRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/myfreecams.png';
    } else if (this is NaverTVRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/navertv.png';
    } else if (this is NimoTVRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/nimotv.png';
    } else if (this is OdnoklassnikiRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/odnoklassniki.png';
    } else if (this is PeriscopeRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/periscope.png';
    } else if (this is PicartoRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/picarto.png';
    } else if (this is SmashCastRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/smashcast.png';
    } else if (this is SteamRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/steam.png';
    } else if (this is SteamMateRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/streamate.png';
    } else if (this is StreamRayRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/streamray.png';
    } else if (this is StripchatRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/stripchat.png';
    } else if (this is TwitchRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/twitch.png';
    } else if (this is VaughnLiveRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/vaughnlive.png';
    } else if (this is VimeoRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/vimeo.png';
    } else if (this is VirtWishRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/virtwish.png';
    } else if (this is VkontakteRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/vkontakte.png';
    } else if (this is VLiveRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/vlive.png';
    } else if (this is XLoveCamRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/xlovecam.png';
    } else if (this is YouTubeRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/youtube.png';
    } else if (this is ZhanqiTVRtmpOut) {
      return 'https://api.fastogt.com/api/static/channels/zhanqitv.png';
    }

    return 'https://api.fastogt.com/api/static/channels/unknown_channel.png';
  }

  List<Tag>? get tags {
    if (this is AfreecaTVRtmpOut) {
      return [Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
    } else if (this is BiliBiliRtmpOut) {
      return [Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
    } else if (this is BongaCamsRtmpOut) {
      return [Tag.ADULT];
    } else if (this is BreakersTVRtmpOut) {
      return [Tag.GAMING, Tag.MUSIC, Tag.VLOG];
    } else if (this is Cam4RtmpOut) {
      return [Tag.ADULT];
    } else if (this is CamPlaceRtmpOut) {
      return [Tag.ADULT];
    } else if (this is CamSodaRtmpOut) {
      return [Tag.ADULT];
    } else if (this is ChaturbateRtmpOut) {
      return [Tag.ADULT];
    } else if (this is CustomRtmpOut) {
      return [Tag.ADULT, Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
    } else if (this is DLiveRtmpOut) {
      return [Tag.GAMING, Tag.VLOG];
    } else if (this is DouyuRtmpOut) {
      return [Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
    } else if (this is FacebookRtmpOut) {
      return [Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
    } else if (this is FC2RtmpOut) {
      return [Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
    } else if (this is Flirt4FreeRtmpOut) {
      return [Tag.ADULT];
    } else if (this is GoodGameRtmpOut) {
      return [Tag.GAMING, Tag.GAMING];
    } else if (this is HuyaRtmpOut) {
      return [Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
    } else if (this is KakaoTVRtmpOut) {
      return [Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
    } else if (this is MixerRtmpOut) {
      return [Tag.GAMING, Tag.MUSIC];
    } else if (this is MyFreeCamsRtmpOut) {
      return [Tag.ADULT];
    } else if (this is NaverTVRtmpOut) {
      return [Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
    } else if (this is NimoTVRtmpOut) {
      return [Tag.GAMING];
    } else if (this is OdnoklassnikiRtmpOut) {
      return [Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
    } else if (this is PeriscopeRtmpOut) {
      return [Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
    } else if (this is PicartoRtmpOut) {
      return [Tag.EDUCATION, Tag.VLOG];
    } else if (this is SmashCastRtmpOut) {
      return [Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
    } else if (this is SteamRtmpOut) {
      return [Tag.GAMING];
    } else if (this is SteamMateRtmpOut) {
      return [Tag.ADULT];
    } else if (this is StreamRayRtmpOut) {
      return [Tag.ADULT];
    } else if (this is StripchatRtmpOut) {
      return [Tag.ADULT];
    } else if (this is TwitchRtmpOut) {
      return [Tag.GAMING, Tag.MUSIC];
    } else if (this is VaughnLiveRtmpOut) {
      return [Tag.GAMING, Tag.MUSIC, Tag.VLOG];
    } else if (this is VimeoRtmpOut) {
      return [Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
    } else if (this is VirtWishRtmpOut) {
      return [Tag.ADULT];
    } else if (this is VkontakteRtmpOut) {
      return [Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
    } else if (this is VLiveRtmpOut) {
      return [Tag.GAMING, Tag.MUSIC, Tag.VLOG];
    } else if (this is XLoveCamRtmpOut) {
      return [Tag.ADULT];
    } else if (this is YouTubeRtmpOut) {
      return [Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
    } else if (this is ZhanqiTVRtmpOut) {
      return [Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
    }

    return [Tag.ADULT, Tag.EDUCATION, Tag.GAMING, Tag.MUSIC, Tag.RELIGION, Tag.VLOG];
  }
}
