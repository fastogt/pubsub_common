class Server {
  static const ID_FIELD = 'id';
  static const NAME_FILED = 'name';
  static const RTMP_ROOT_FILED = 'rtmp_root';
  static const LOAD_FIELD = 'load';

  final String? id;
  final String name;
  final String rtmpRoot;
  final double? load;

  Server({required this.id, required this.name, required this.rtmpRoot, required this.load});

  Map<String, dynamic> toJson() {
    return {ID_FIELD: id, NAME_FILED: name, RTMP_ROOT_FILED: rtmpRoot, LOAD_FIELD: load};
  }

  bool isValid() {
    if (id?.isEmpty ?? true) {
      return false;
    }
    return rtmpRoot.isNotEmpty;
  }

  Server copy() {
    return Server(id: id, name: name, rtmpRoot: rtmpRoot, load: load);
  }

  factory Server.fromJson(Map<String, dynamic> json) {
    final sid = json[Server.ID_FIELD];
    final name = json[Server.NAME_FILED];
    final root = json[Server.RTMP_ROOT_FILED];
    final load = json[Server.LOAD_FIELD];
    return Server(id: sid, name: name, rtmpRoot: root, load: load);
  }

  String generatePublishRtmpUrl(String streamName, String uid,
      [double? latitude, double? longitude]) {
    final StringBuffer _buf = StringBuffer('$rtmpRoot/$streamName?sid=$id&uid=$uid');

    if (latitude != null && longitude != null) {
      _buf.write('&latitude=$latitude&longitude=$longitude');
    }

    return _buf.toString();
  }

  String generatePlayRtmpUrl(String streamName, String uid) {
    return '$rtmpRoot/$streamName';
  }
}

Server? findBestServer(List<Server> servers) {
  Server? result;
  for (final Server ser in servers) {
    result ??= ser;
    if (result.load! > ser.load!) {
      result = ser;
    }
  }
  return result;
}
